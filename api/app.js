const express = require("express");
const app = express();
// const server = require("http").createServer(app);
// const io = require("socket.io")(server);
const cors = require("cors");
const mongoose = require("mongoose");

const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./.env") });
const session = require("express-session");
const rateLimiter = require("express-rate-limit");

const sessionRoute = require("./routes/session");
const authRoute = require("./routes/auth");
const dogsRoute = require("./routes/dogs");
const matchesRoute = require("./routes/matches");

// ------------ MIDDLEWARE ------------ //
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
  session({
    name: process.env.SESSION_NAME,
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false },
  })
);

app.use(
  rateLimiter({
    windowMs: 10 * 60 * 1000,
    max: 200,
  })
);

app.use(
  "/auth/",
  rateLimiter({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 10,
  })
);

// --------------- ROUTES --------------- //

app.use("/session", sessionRoute);
app.use("/auth", authRoute);
app.use("/dogs", dogsRoute);
app.use("/matches", matchesRoute);

// ----------- DB CONNECTION ----------- //

// mongodb atlas connection
mongoose.connect(
  process.env.MONGODB_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  },
  (err, client) => {
    if (err) return console.log(err);
    console.log("connected");
  }
);

// ------------ SOCKET.IO ------------ //

// const matchchatNamespace = io.of("/matchchat");

// matchchatNamespace.on("connection", (socket) => {
//   console.log(`connect: ${socket.id}`);

//   socket.on("message", ({ data }) => {
//     console.log(data);
//     console.log(`hello from ${socket.id}`);
//     matchchatNamespace.emit({ data: "hi from backend" });
//   });

//   socket.on("disconnect", () => {
//     console.log("disconnected");
//   });
// });

// ------------ RUN SERVER ------------ //

const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log("Server is running on port ", port);
});
