const router = require("express").Router();
const { PlayMatchModel } = require("../models/PlayMatch");
const { DogModel } = require("../models/Dog");
const multer = require("multer");
const upload = multer();

// get all matches
router.get("/", async (req, res) => {
  try {
    const matches = await PlayMatchModel.find({});
    return res.send({ matchData: matches });
  } catch (error) {
    return res.send({ error });
  }
});

// get match by matchid
router.get("/:matchid", async (req, res) => {
  const { matchid } = req.params;
  try {
    const match = await PlayMatchModel.findOne({ _id: matchid })
      .populate({ path: "dog1" })
      .populate({ path: "dog2" })
      .exec();
    return res.send({ matchData: match });
  } catch (error) {
    return res.send({ error });
  }
});

// get match by dogid
router.get("/dog/:dogid", async (req, res) => {
  const { dogid } = req.params;
  try {
    const matches = await PlayMatchModel.find({
      $or: [{ dog1: dogid }, { dog2: dogid }],
    })
      .populate({ path: "dog1" })
      .populate({ path: "dog2" })
      .exec();
    return res.send({ matchData: matches });
  } catch (error) {
    return res.send({ error });
  }
});

// add new match
router.post("/add", upload.none(), async (req, res) => {
  const { dogId1, dogId2, playTime } = req.body;

  try {
    const newMatch = await new PlayMatchModel({
      dog1: dogId1,
      dog2: dogId2,
      playTime: playTime,
      accepted: false,
    });
    await newMatch.save(async (err) => {
      if (err) return handleError(err);
      await DogModel.updateMany(
        { _id: { $in: [dogId1, dogId2] } },
        { $push: { matches: newMatch._id } },
        { new: true }
      );
      return;
    });

    return res.send({ matchData: newMatch });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// update match accepted status
router.post("/:matchid", upload.none(), async (req, res) => {
  const { matchid } = req.params;

  try {
    const updatedMatch = await PlayMatchModel.findOneAndUpdate(
      { _id: matchid },
      { $set: { accepted: true } },
      { new: true }
    )
      .populate({ path: "dog1" })
      .populate({ path: "dog2" })
      .exec();
    return res.send({ matchData: updatedMatch });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// // delete match
router.delete("/:matchid", upload.none(), async (req, res) => {
  try {
    const { matchid } = req.params;
    try {
      await PlayMatchModel.deleteOne({ _id: matchid });
      const updatedMatches = await PlayMatchModel.find({});
      return res.send({ matchData: updatedMatches });
    } catch (error) {
      return res.send({ error });
    }
  } catch (error) {
    return res.status(500).send({ error });
  }
});

module.exports = router;
