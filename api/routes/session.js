const router = require("express").Router();
const UserModel = require("../models/User");

router.get("/getuser", async (req, res) => {
  const { userId } = req.session;
  console.log(userId);

  if (userId) {
    const user = await UserModel.findOne({ _id: userId })
      .populate({ path: "dogs", populate: { path: "matches" } })
      .exec();
    return res.send({ user });
  }
  return res.send({ user: null });
});

module.exports = router;
