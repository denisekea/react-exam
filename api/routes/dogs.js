const router = require("express").Router();
const { DogModel } = require("../models/Dog");
const UserModel = require("../models/User");
const multer = require("multer");
const upload = multer();

// get all dogs
router.get("/", async (req, res) => {
  try {
    const dogs = await DogModel.find({});
    return res.send({ dogData: dogs });
  } catch (error) {
    console.log(error);
    return res.send({ error });
  }
});

// get single dog
router.get("/:dogid", async (req, res) => {
  const { dogid } = req.params;

  try {
    const dog = await DogModel.findOne({ _id: dogid })
      .populate({ path: "matches" })
      .exec();
    return res.send({ dogData: dog });
  } catch (error) {
    console.log(error);
    return res.send({ error });
  }
});

// get dogs by userid
router.get("/user/:userid", async (req, res) => {
  const { userid } = req.params;

  try {
    const dogs = await DogModel.find({ userid: userid })
      .populate({ path: "matches" })
      .exec();
    return res.send({ dogData: dogs });
  } catch (error) {
    console.log(error);
    return res.send({ error });
  }
});

// add new dog
router.post("/add", upload.none(), async (req, res) => {
  const {
    dogName,
    age,
    gender,
    breed,
    description,
    lookingFor,
    imageURL,
    userId,
  } = req.body;

  const newDogObject = {
    dogName,
    age,
    gender,
    breed,
    description,
    lookingFor,
    imageURL,
    userid: userId,
    matches: [],
  };

  try {
    const newDog = await new DogModel(newDogObject);
    await newDog.save(async (err) => {
      if (err) return handleError(err);
      await UserModel.findOneAndUpdate(
        { _id: userId },
        { $push: { dogs: newDog._id } },
        { new: true }
      );
      return;
    });

    return res.send({ dogData: newDog });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// update dog
router.post("/:dogid", upload.none(), async (req, res) => {
  const { dogid } = req.params;

  const {
    dogName,
    age,
    gender,
    breed,
    description,
    lookingFor,
    imageURL,
  } = req.body;
  console.log("update", dogid, dogName);

  try {
    await DogModel.findOneAndUpdate(
      { _id: dogid },
      {
        $set: {
          dogName: dogName,
          age: age,
          gender: gender,
          breed: breed,
          description: description,
          lookingFor: lookingFor,
          imageURL: imageURL,
        },
      },
      { new: true }
    );
    const updatedDogs = await DogModel.find({});
    return res.send({ dogData: updatedDogs });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// delete dog
router.delete("/:dogid", upload.none(), async (req, res) => {
  try {
    const { dogid } = req.params;
    try {
      await DogModel.deleteOne({ _id: dogid });
      const updatedDogs = await DogModel.find({});
      return res.send({ dogData: updatedDogs });
    } catch (error) {
      return res.send({ error });
    }
  } catch (error) {
    return res.status(500).send({ error });
  }
});

module.exports = router;
