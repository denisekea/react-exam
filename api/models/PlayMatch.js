const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const PlayMatchSchema = new Schema(
  {
    dog1: { type: Schema.Types.ObjectId, ref: "Dog", required: true },
    dog2: { type: Schema.Types.ObjectId, ref: "Dog", required: true },
    playTime: { type: Date },
    accepted: { type: Boolean },
  },
  { timestamps: true }
);

PlayMatchSchema.plugin(uniqueValidator, { message: "already exists" });

const PlayMatchModel = mongoose.model("PlayMatch", PlayMatchSchema);

module.exports.PlayMatchSchema = PlayMatchSchema;
module.exports.PlayMatchModel = PlayMatchModel;
