const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const DogSchema = new Schema(
  {
    dogName: {
      type: String,
      required: [true, "can't be blank"],
    },
    imageURL: { type: String },
    age: { type: Number },
    gender: { type: String },
    breed: { type: String },
    description: { type: String },
    lookingFor: { type: String },
    userid: { type: Schema.Types.ObjectId, ref: "User" },
    matches: [{ type: Schema.Types.ObjectId, ref: "PlayMatch" }],
  },
  { timestamps: true }
);

DogSchema.plugin(uniqueValidator, { message: "already exists" });

const DogModel = mongoose.model("Dog", DogSchema, "dogs");

module.exports.DogSchema = DogSchema;
module.exports.DogModel = DogModel;
