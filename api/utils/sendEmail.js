const nodemailer = require("nodemailer");

async function sendEmail(mailObj) {
  const { toEmail, subject, text } = mailObj;
  const {
    EMAIL_HOST,
    EMAIL_PORT,
    EMAIL_HOST_USER,
    EMAIL_HOST_PASSWORD,
  } = process.env;

  console.log(EMAIL_HOST);

  try {
    let transporter = nodemailer.createTransport({
      host: EMAIL_HOST,
      port: EMAIL_PORT,
      auth: {
        user: EMAIL_HOST_USER,
        pass: EMAIL_HOST_PASSWORD,
      },
      secure: false,
      logger: true,
      debug: true,
    });

    let info = await transporter.sendMail({
      from: '"Denise 👻" <denisetan@live.nl>',
      to: toEmail,
      subject: subject,
      html: `<h1>${text}</h1>`,
    });

    console.log(`Message sent: ${info.messageId}`);
    return `Message sent: ${info.messageId}`;
  } catch (error) {
    console.error(error);
    throw new Error(
      `Something went wrong in the sendmail method. Error: ${error.message}`
    );
  }
}

module.exports = sendEmail;
