# react-exam

[http://denisekea.gitlab.io/react-exam/](http://denisekea.gitlab.io/react-exam/)

## Run backend

cd api

yarn / npm install

node app.js

## Run React app

cd pupplay

yarn / npm install

yarn start / npm start
