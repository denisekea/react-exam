import React, { useEffect, useState } from "react";
import styled, { ThemeProvider } from "styled-components";
import { BrowserRouter as Router } from "react-router-dom";
import { API_ROOT } from "../endpoints";
import AuthContext from "../common/AuthContext/AuthContext";
import Loader from "../common/Loader/Loader";
import Routes from "../routes/Routes";

const theme = {
  greenLight: "#69f6b7",
  greenDark: "#329D9C",
  headerColor: "#fcc391",
  indigo: "#583eb2",
};

const AppContainer = styled("div")`
  position: relative;
  background: linear-gradient(
    346deg,
    rgba(105, 246, 183, 1) 0%,
    rgba(50, 157, 156, 1) 80%
  );
  height: 100%;
  margin: 0;
`;

const App = () => {
  const [loading, setLoading] = useState();
  const [auth, setAuth] = useState();

  const handleAuthChange = (authState) => {
    setAuth(authState);
  };

  const getSession = async () => {
    try {
      const res = await fetch(`${API_ROOT}/session/getuser`, {
        method: "GET",
        credentials: "include",
        mode: "no-cors",
      });
      const data = await res.json();

      if (data && data.user) {
        await setAuth(data.user);
        setLoading(false);
      } else {
        setAuth(false);
        setLoading(false);
      }
    } catch (e) {
      setAuth(false);
      setLoading(false);
    }
  };

  useEffect(() => {
    setLoading(true);
    getSession();
  }, []);

  if (loading || auth === undefined) {
    return (
      <AppContainer>
        <Loader />
      </AppContainer>
    );
  }

  return (
    <AuthContext.Provider value={{ auth, handleAuthChange }}>
      <ThemeProvider theme={theme}>
        <Router basename="/react-exam">
          <AppContainer>
            <Routes />
          </AppContainer>
        </Router>
      </ThemeProvider>
    </AuthContext.Provider>
  );
};

export default App;
