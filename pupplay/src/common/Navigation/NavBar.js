import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { FiLogOut as LogOutIcon } from "react-icons/fi";
import { API_ROOT } from "../../endpoints";
import AuthContext from "../AuthContext/AuthContext";

const NavContainer = styled("div")`
  position: relative;
  width: 350px;
  text-align: right;
  margin: 2em 3em 2em auto;
  vertical-align: middle;
  display: flex;
  justify-content: flex-end;

  div {
    display: inline-block;
    color: ${(props) => props.theme.greenLight};
    border-bottom: 2px solid ${(props) => props.theme.greenLight};
    background-color: transparent;
    font-weight: bold;
    font-size: 1.2em;
    margin: 0 1em;
    padding-bottom: 2px;
    cursor: pointer;

    &:hover {
      color: ${(props) => props.theme.headerColor};
      border-bottom: 2px solid ${(props) => props.theme.headerColor};
    }
  }
`;

const LogOutButton = styled(LogOutIcon)`
  height: 20px;
  width: 20px;
  margin: 2px 1em 0;

  path,
  polyline,
  line {
    stroke: ${(props) => props.theme.greenLight};
    stroke-width: 3;
  }
  cursor: pointer;

  &:hover {
    path,
    polyline,
    line {
      stroke: ${(props) => props.theme.headerColor};
    }
  }
`;

const NavBar = () => {
  const { handleAuthChange } = useContext(AuthContext);
  const history = useHistory();

  const handleLogout = async () => {
    const connection = await fetch(`${API_ROOT}/auth/logout`);
    const data = await connection.json();
    if (data.error) {
      console.log("couldn't log out");
      return;
    }
    handleAuthChange(false);
    localStorage.removeItem("uid");
    history.push("/logout");
  };

  return (
    <NavContainer>
      <div onClick={() => history.push("/dashboard")}>Dashboard</div>
      <div onClick={() => history.push("/matchfinder")}>Match Finder</div>
      <LogOutButton onClick={handleLogout} />
    </NavContainer>
  );
};

export default NavBar;
