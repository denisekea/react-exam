import React from "react";

const CheckMarkIcon = (props) => {
  return (
    <svg
      {...props}
      viewBox="0 0 24 24"
      fill="none"
      stroke="black"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round">
      <polyline fill="none" points="20 6 9 17 4 12" />
    </svg>
  );
};

export default CheckMarkIcon;
