import React, { useState } from "react";

const DogForm = ({ currentDog, handleOnSubmit }) => {
  const [formData, setFormData] = useState({ ...currentDog });
  console.log(formData);
  const {
    dogName,
    age,
    gender,
    breed,
    description,
    lookingFor,
    imageURL,
  } = currentDog;

  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    handleOnSubmit(formData);
  };

  return (
    <form method="POST" onSubmit={handleFormSubmit}>
      <label htmlFor="dogName">Name</label>
      <input
        name="dogName"
        type="text"
        defaultValue={dogName || ""}
        onChange={handleInputChange}
        required
      />

      <label htmlFor="age">Age (in years)</label>
      <input
        name="age"
        type="number"
        step="0.1"
        defaultValue={age || ""}
        onChange={handleInputChange}
        required
      />

      <label htmlFor="gender">Gender</label>
      <select
        name="gender"
        type="text"
        defaultValue={gender || "female"}
        onChange={handleInputChange}
        required>
        <option value="female">Female</option>
        <option value="male">Male</option>
      </select>

      <label htmlFor="breed">Breed</label>
      <input
        name="breed"
        type="text"
        defaultValue={breed || ""}
        onChange={handleInputChange}
        required
      />

      <label htmlFor="description">Description</label>
      <input
        name="description"
        type="textarea"
        defaultValue={description || ""}
        onChange={handleInputChange}
        required
      />

      <label htmlFor="lookingFor">Looking for</label>
      <input
        name="lookingFor"
        type="text"
        defaultValue={lookingFor || ""}
        onChange={handleInputChange}
        required
      />

      <label htmlFor="imageURL">Image URL</label>
      <input
        name="imageURL"
        type="text"
        defaultValue={imageURL || ""}
        onChange={handleInputChange}
      />
      <button>Save</button>
    </form>
  );
};

export default DogForm;
