import React, { useState } from "react";
import styled from "styled-components";
import searchIcon from "../assets/searchIcon.svg";

const SearchContainer = styled("form")`
  height: 37px;
  width: 35%;
  margin-bottom: 25px;
`;

const SearchInput = styled("input")`
  width: 100%;
  border-radius: 5px;
  border: none;
  background-color: #efeafd;
  padding: 5px 9% 5px 7px;
  background: #efeafd url(${searchIcon}) no-repeat right / 10% 65%;
  margin-bottom: 5px;

  &:focus {
    outline: none;
    border-bottom: none;
    box-shadow: 0 0 0 3px ${(props) => props.theme.indigo};
  }
`;

const ResetInput = styled("input")`
  height: auto;
  border: none;
  background-color: transparent;
  cursor: pointer;
  font-size: 0.9em;
  float: right;
  width: 7em;
  margin: 0;
  padding: 0;
  text-align: right;

  &:hover {
    color: ${(props) => props.theme.greenLight};
  }

  &:focus {
    outline: none;
    box-shadow: none;
  }
`;

const SearchBar = ({ searchActive, triggerSearch, clearSearch }) => {
  const [searchValue, setSearchValue] = useState("");

  return (
    <SearchContainer
      onSubmit={(e) => {
        e.preventDefault();
      }}
      onReset={clearSearch}>
      <SearchInput
        type="text"
        placeholder="Search for a play match..."
        onChange={(e) => setSearchValue(e.target.value)}
        onKeyDown={(e) => {
          e.key === "Enter" && searchValue && triggerSearch(searchValue);
        }}
      />
      {searchActive && <ResetInput type="reset" value="clear search" />}
    </SearchContainer>
  );
};

export default SearchBar;
