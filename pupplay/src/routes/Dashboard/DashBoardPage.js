import React, { useContext, useState } from "react";
import styled from "styled-components";
import NavBar from "../../common/Navigation/NavBar";
import AuthContext from "../../common/AuthContext/AuthContext";
import SavedConfirmation from "../../common/Notification/SavedConfirmation";
import UserDogs from "./components/UserDogs";
import UserProfile from "./components/UserProfile";
import MainLoader from "../../common/Loader/Loader";

const DashboardContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;
`;

const ContentContainer = styled("div")`
  padding: 0 10% 3em;

  h1 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.headerColor};
    margin: 0;
    margin-bottom: 1em;
    font-size: 2.7em;
    font-weight: 700;
  }
`;

const DashboardPage = () => {
  const [notification, setNotification] = useState(false);
  const { auth, handleAuthChange } = useContext(AuthContext);
  console.log(auth);

  const displayNotification = () => {
    setNotification(true);
    setTimeout(() => setNotification(false), 2000);
  };

  if (!auth) {
    return <MainLoader />;
  }

  return (
    <DashboardContainer>
      <NavBar />
      <ContentContainer>
        <h1>Dashboard</h1>
        <UserDogs
          dogs={auth.dogs}
          userId={auth._id}
          displayNotification={displayNotification}
        />
        <UserProfile
          user={auth}
          handleAuthChange={handleAuthChange}
          displayNotification={displayNotification}
        />
      </ContentContainer>
      {notification && <SavedConfirmation text={"Saved"} />}
    </DashboardContainer>
  );
};

export default DashboardPage;
