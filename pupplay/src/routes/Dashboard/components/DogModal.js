import React from "react";
import styled from "styled-components";
import { API_ROOT } from "../../../endpoints";
import DogForm from "../../../common/DogForm/DogForm";

const Overlay = styled("div")`
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  background-color: rgba(116, 118, 129, 0.8);
  width: 100%;
  z-index: 10;
`;

const ModalContainer = styled("div")`
  position: relative;
  width: 40%;
  height: 72%;
  margin: 5em auto;
  background-color: white;
  border-radius: 10px;
  opacity: 1;
  padding: 3em;
  overflow: auto;

  ::-webkit-scrollbar {
    width: 5px;
  }
  ::-webkit-scrollbar-track {
    background: transparent;
  }
  ::-webkit-scrollbar-thumb {
    background-color: darkgrey;
    border-radius: 30px;
  }

  h2 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.indigo};
    margin: 0;
    margin-bottom: 1em;
  }
`;

export const CloseButton = styled("div")`
  position: absolute;
  top: 9px;
  right: 11px;
  border-radius: 5px;
  padding: 5px;
  width: 1em;
  text-align: center;
  font-family: "Montserrat", sans-serif;
  color: ${(props) => props.theme.indigo};
  font-weight: bold;
  font-size: 1.2em;
  cursor: pointer;
`;

const DogModal = ({
  selectedDog,
  handleCloseModal,
  handleUpdateUserDogs,
  isNew,
  displayNotification,
}) => {
  console.log(selectedDog);

  const handleOnSubmit = async (formData) => {
    console.log(formData);
    const apiEndpoint = isNew ? "/dogs/add" : `/dogs/${selectedDog._id}`;

    const connection = await fetch(`${API_ROOT + apiEndpoint}`, {
      method: "POST",
      body: JSON.stringify(formData),
      headers: { "Content-Type": "application/json" },
    });
    const data = await connection.json();

    if (data.dogData) {
      displayNotification(true);
      handleUpdateUserDogs();
    }
  };

  return (
    <Overlay>
      <ModalContainer>
        <h2>{isNew ? "Add new dog" : "Edit dog"}</h2>
        <DogForm currentDog={selectedDog} handleOnSubmit={handleOnSubmit} />
        <CloseButton onClick={handleCloseModal}>X</CloseButton>
      </ModalContainer>
    </Overlay>
  );
};

export default DogModal;
