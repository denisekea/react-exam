import React, { useState, useContext } from "react";
import styled from "styled-components";
import { API_ROOT } from "../../../endpoints";
import AuthContext from "../../../common/AuthContext/AuthContext";
import DogModal from "./DogModal";
import SingleUserDog from "./SingleUserDog";

const DogsContainer = styled("div")`
  margin-bottom: 4em;
`;

const DogsHeader = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 2em;

  h2 {
    display: inline-block;
    width: 7em;
    font-family: "Montserrat", sans-serif;
    margin: 0;
    color: ${(props) => props.theme.indigo};
    font-size: 1.8em;
    font-weight: 500;
  }
`;

const AddDogButton = styled("button")`
  background-color: ${(props) => props.theme.indigo};
  color: ${(props) => props.theme.headerColor};
  &:hover {
    background-color: ${(props) => props.theme.headerColor};
    color: ${(props) => props.theme.indigo};
  }
`;

const UserDogGrid = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 1em;
`;

const UserDogs = ({ dogs, userId, displayNotification }) => {
  const [selectedDog, setSelectedDog] = useState({});
  const [modalOpened, setModalOpened] = useState(false);
  const [isNew, setIsNew] = useState(false);
  const { auth, handleAuthChange } = useContext(AuthContext);

  const newDog = {
    dogName: "",
    age: 0,
    gender: "female",
    breed: "",
    description: "",
    lookingFor: "",
    imageURL: "",
    matches: [],
    userId,
  };

  const handleAddDog = () => {
    setModalOpened(true);
    setSelectedDog(newDog);
    setIsNew(true);
  };

  const handleCloseModal = () => {
    setModalOpened(false);
    setSelectedDog({});
    setIsNew(false);
  };

  const handleDogSelection = (dog) => {
    setSelectedDog(dog);
    setModalOpened(true);
  };

  const handleUpdateUserDogs = async () => {
    const connection = await fetch(`${API_ROOT}/dogs/user/${userId}`);
    const data = await connection.json();
    if (data.dogData) {
      handleAuthChange({ ...auth, dogs: data.dogData });
    }
  };

  return (
    <>
      <DogsContainer>
        <DogsHeader>
          <h2>Your dogs</h2>
          <AddDogButton onClick={handleAddDog}>Add new dog</AddDogButton>
        </DogsHeader>
        <UserDogGrid>
          {(dogs.length &&
            dogs.map((dog) => (
              <SingleUserDog
                key={dog._id}
                dog={dog}
                handleDogSelection={handleDogSelection}
              />
            ))) || <p>No dogs yet</p>}
        </UserDogGrid>
      </DogsContainer>
      {modalOpened && (
        <DogModal
          selectedDog={selectedDog}
          handleCloseModal={handleCloseModal}
          handleUpdateUserDogs={handleUpdateUserDogs}
          isNew={isNew}
          displayNotification={displayNotification}
        />
      )}
    </>
  );
};

export default UserDogs;
