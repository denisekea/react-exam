import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { API_ROOT } from "../../../endpoints";
import DogAvatar from "../../../common/assets/avatar_placeholder.svg";
import MatchesDisplay from "./MatchesDisplay";

const DogRow = styled("div")`
  display: grid;
  grid-template-columns: 180px 1fr;
  column-gap: 1.5em;
  background-color: white;
  border-radius: 10px;
  padding: 1.5em;

  .dog-avatar {
    height: 170px;
    width: 170px;
    border-radius: 7px;
    object-fit: cover;
    margin: 0 auto;
  }

  p {
    margin: 10px 0;

    &:first-of-type {
      font-size: 1.4em;
      font-weight: bold;
    }
  }
`;

const EditButton = styled("button")`
  width: 170px;
  justify-self: center;
  background-color: #a6f9d4;
  color: ${(props) => props.theme.indigo};
  margin-top: 2px;

  &:hover {
    background-color: ${(props) => props.theme.greenLight};
    color: ${(props) => props.theme.indigo};
  }
`;

const SingleUserDog = ({ dog, handleDogSelection }) => {
  const { _id, dogName, age, gender, breed, imageURL } = dog;
  const [matches, setMatches] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleEditClick = () => {
    handleDogSelection(dog);
  };

  useEffect(() => {
    const fetchMatches = async () => {
      const res = await fetch(`${API_ROOT}/matches/dog/${_id}`);
      const data = await res.json();
      if (data.matchData) {
        console.log(data.matchData);
        setMatches(data.matchData);
        setLoading(false);
      }
    };

    setLoading(true);
    fetchMatches();
  }, [_id]);

  if (loading) {
    return <></>;
  }

  return (
    <DogRow>
      <div>
        <img
          className="dog-avatar"
          src={imageURL || DogAvatar}
          alt="dog avatar"
        />
        <EditButton onClick={handleEditClick}>Edit dog</EditButton>
      </div>
      <div>
        <p>{dogName}</p>
        <p>
          {age} years &#8226; {gender} &#8226; {breed}
        </p>
        <MatchesDisplay key={_id} currentDog={_id} matches={matches} />
      </div>
    </DogRow>
  );
};

export default SingleUserDog;
