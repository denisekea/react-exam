import React from "react";
import styled from "styled-components";
import SingleMatch from "./SingleMatch";

const MatchesContainer = styled("div")`
  margin-top: 1em;
`;

const MatchesDisplay = ({ currentDog, matches }) => {
  const sortedMatches = matches.sort(function (a, b) {
    return new Date(a.playTime) - new Date(b.playTime);
  });

  return (
    <MatchesContainer>
      <p>Play matches:</p>
      {(sortedMatches.length > 0 &&
        sortedMatches.map((match) => {
          return (
            <SingleMatch
              key={match._id}
              match={match}
              currentDog={currentDog}
            />
          );
        })) || <p>No matches yet</p>}
    </MatchesContainer>
  );
};

export default MatchesDisplay;
