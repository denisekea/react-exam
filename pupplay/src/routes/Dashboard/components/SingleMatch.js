import React from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";

const SingleMatchContainer = styled("div")`
  color: ${(props) => props.theme.indigo};
  font-weight: bold;
  margin: 3px 0;
  cursor: pointer;

  &:hover {
    color: ${(props) => props.theme.indigo};
    background-color: #a6f9d4;
  }

  span {
    font-weight: normal;
  }
`;

const SingleMatch = ({ currentDog, match }) => {
  const { dog1, dog2, playTime, accepted } = match;
  const matchedDog = currentDog === dog1._id ? dog2 : dog1;
  const history = useHistory();
  const parsedDate = new Date(playTime).toLocaleDateString(undefined, {
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  return (
    <SingleMatchContainer
      onClick={() => {
        console.log("single match");
        history.push(`/matchpage/${match._id}`);
      }}>
      <div>
        {parsedDate} with {matchedDog.dogName}
        <span> &#8226; {accepted ? "confirmed" : "unconfirmed"}</span>
      </div>
    </SingleMatchContainer>
  );
};

export default SingleMatch;
