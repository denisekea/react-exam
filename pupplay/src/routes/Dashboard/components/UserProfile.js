import React from "react";
import styled from "styled-components";
import { API_ROOT } from "../../../endpoints";

const ProfileContainer = styled("div")`
  h2 {
    font-family: "Montserrat", sans-serif;
    margin: 0;
    color: #583eb2;
    margin-bottom: 1em;
    font-size: 1.8em;
    font-weight: 500;
  }
`;

const ProfileHeader = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 2em;

  h2 {
    display: inline-block;
    width: 7em;
    font-family: "Montserrat", sans-serif;
    margin: 0;
    color: ${(props) => props.theme.indigo};
    font-size: 1.8em;
    font-weight: 500;
  }
`;

const DeleteButton = styled("button")`
  background-color: ${(props) => props.theme.indigo};
  color: ${(props) => props.theme.headerColor};
  &:hover {
    background-color: ${(props) => props.theme.headerColor};
    color: ${(props) => props.theme.indigo};
  }
`;

const Profile = styled("div")`
  background-color: white;
  border-radius: 10px;
  padding: 1.5em;
`;

const UserProfile = ({ user, handleAuthChange, displayNotification }) => {
  const handleDeleteProfile = async () => {
    const { _id } = user;
    const connection = await fetch(`${API_ROOT}/auth/${_id}`, {
      method: "DELETE",
    });
    const data = await connection.json();
    if (data.dCount) {
      displayNotification();
      setTimeout(() => {
        handleAuthChange(false);
      }, 2200);
    } else {
      console.log(data.error);
    }
  };

  return (
    <ProfileContainer>
      <ProfileHeader>
        <h2>Your profile</h2>
        <DeleteButton onClick={handleDeleteProfile}>
          Delete profile
        </DeleteButton>
      </ProfileHeader>
      <Profile>
        <p>Name: {user.fullName}</p>
        <p>Email: {user.email}</p>
      </Profile>
    </ProfileContainer>
  );
};

export default UserProfile;
