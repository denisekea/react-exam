import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import AuthContext from "../common/AuthContext/AuthContext";
import LoginPage from "./Auth/LoginPage";
import LogoutPage from "./Auth/LogoutPage";
import DashboardPage from "./Dashboard/DashBoardPage";
import MatchFinderPage from "./MatchFinder/MatchFinderPage";
import MatchPage from "./MatchPage/MatchPage";

const Routes = () => {
  const { auth } = useContext(AuthContext);

  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="/dashboard" />
      </Route>
      <Route
        path="/login"
        component={() => (auth ? <Redirect to="/dashboard" /> : <LoginPage />)}
      />
      <Route path="/logout" component={() => <LogoutPage />} />
      <Route
        path="/dashboard"
        component={() => (auth ? <DashboardPage /> : <Redirect to="/login" />)}
      />
      <Route
        path="/matchfinder"
        component={() =>
          auth ? <MatchFinderPage /> : <Redirect to="/login" />
        }
      />
      <Route
        path="/matchpage/:matchid"
        component={() => (auth ? <MatchPage /> : <Redirect to="/login" />)}
      />
    </Switch>
  );
};

export default Routes;
