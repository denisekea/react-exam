import React, { useState } from "react";
import styled from "styled-components";
import DogCard from "./DogCard";
import MatchModal from "./MatchModal";

const MatchGridContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  grid-gap: 1em;
  margin-top: 2em;
`;

const MatchGrid = ({ displayedMatches, displayNotification, userDogs }) => {
  const [selectedDog, setSelectedDog] = useState({});
  const [modalOpened, setModalOpened] = useState(false);

  const handleOpenModal = (dog) => {
    setModalOpened(true);
    setSelectedDog(dog);
  };

  const handleCloseModal = () => {
    setModalOpened(false);
    setSelectedDog({});
  };

  return (
    <MatchGridContainer>
      {(displayedMatches.length &&
        displayedMatches.map((dog) => {
          return (
            <DogCard
              key={dog._id}
              dog={dog}
              handleOpenModal={handleOpenModal}
            />
          );
        })) ||
        "No dogs available"}
      {modalOpened && (
        <MatchModal
          selectedDog={selectedDog}
          userDogs={userDogs}
          handleCloseModal={handleCloseModal}
          displayNotification={displayNotification}
        />
      )}
    </MatchGridContainer>
  );
};

export default MatchGrid;
