import React from "react";
import styled from "styled-components";

const DogCardContainer = styled("div")`
  position: relative;
  background: white;
  border-radius: 5px;
  padding: 1.5em 2em 4em;

  h3 {
    margin: 0;
  }

  p {
    margin: 0.5em 0;
  }

  img {
    height: 180px;
    width: 100%;
    border-radius: 7px;
    object-fit: cover;
    margin: 0 auto;
  }

  button {
    position: absolute;
    bottom: 1.2em;
  }

  .bold {
    font-weight: bold;
  }
`;

const DogCard = ({ dog, handleOpenModal }) => {
  const {
    dogName,
    age,
    gender,
    breed,
    imageURL,
    description,
    lookingFor,
  } = dog;
  return (
    <DogCardContainer>
      <h3>{dogName}</h3>
      <p>
        {age} years &#8226; {gender}
      </p>
      <img src={imageURL} alt="dog avatar" />
      <p>
        <span className="bold">Breed:</span> {breed}
      </p>
      <p>
        <span className="bold">Description:</span> {description}
      </p>
      <p>
        <span className="bold">Looking for:</span> {lookingFor}
      </p>
      <button onClick={() => handleOpenModal(dog)}>Invite to play</button>
    </DogCardContainer>
  );
};

export default DogCard;
