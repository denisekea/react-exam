import React, { useState } from "react";
import styled from "styled-components";
import { API_ROOT } from "../../../endpoints";

const Overlay = styled("div")`
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  background-color: rgba(116, 118, 129, 0.8);
  width: 100%;
  z-index: 10;
`;

const ModalContainer = styled("div")`
  position: relative;
  width: 450px;
  height: 50%;
  margin: 8em auto;
  background-color: white;
  border-radius: 10px;
  opacity: 1;
  padding: 3em;
  overflow: auto;

  ::-webkit-scrollbar {
    width: 5px;
  }
  ::-webkit-scrollbar-track {
    background: transparent;
  }
  ::-webkit-scrollbar-thumb {
    background-color: darkgrey;
    border-radius: 30px;
  }

  h2 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.indigo};
    margin: 1em 0;
  }

  form {
    width: 60%;
    margin: 4em auto 2em;
  }
  button {
    width: 100%;
  }
`;

export const CloseButton = styled("div")`
  position: absolute;
  top: 9px;
  right: 11px;
  border-radius: 5px;
  padding: 5px;
  width: 1em;
  text-align: center;
  font-family: "Montserrat", sans-serif;
  color: ${(props) => props.theme.indigo};
  font-weight: bold;
  font-size: 1.2em;
  cursor: pointer;
`;

const MatchModal = ({
  selectedDog,
  userDogs,
  handleCloseModal,
  displayNotification,
}) => {
  const [formData, setFormData] = useState({
    dogId1: userDogs[0]._id || "",
    dogId2: selectedDog._id,
    playTime: "",
  });
  console.log(formData);

  const handleInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleOnSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`${API_ROOT}/matches/add`, {
        method: "POST",
        body: JSON.stringify(formData),
        headers: { "Content-Type": "application/json" },
      });
      const data = await response.json();
      console.log(data);
      if (data.matchData) {
        displayNotification(true);
        handleCloseModal();
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Overlay>
      <ModalContainer>
        <h2>Plan a new play date with {selectedDog.dogName}</h2>
        <form method="POST" onSubmit={handleOnSubmit}>
          <label htmlFor="dogId1">For: </label>
          <select name="dogId1" onChange={handleInputChange}>
            {userDogs.map((userDog) => {
              return (
                <option key={"option" + userDog._id} value={userDog._id}>
                  {userDog.dogName}
                </option>
              );
            })}
          </select>
          <label htmlFor="playTime">On: </label>
          <input
            name="playTime"
            type="datetime-local"
            onChange={handleInputChange}
          />
          <button>Invite for play date</button>
        </form>
        <CloseButton onClick={handleCloseModal}>X</CloseButton>
      </ModalContainer>
    </Overlay>
  );
};

export default MatchModal;
