import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import { API_ROOT } from "../../endpoints";
import AuthContext from "../../common/AuthContext/AuthContext";
import NavBar from "../../common/Navigation/NavBar";
import SearchBar from "../../common/SearchBar/SearchBar";
import Loader from "../../common/Loader/Loader";
import SavedConfirmation from "../../common/Notification/SavedConfirmation";
import MatchGrid from "./components/MatchGrid";

const MatchFinderContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;
`;

const ContentContainer = styled("div")`
  padding: 0 10% 3em;

  h1 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.headerColor};
    margin: 0;
    margin-bottom: 1em;
    font-size: 2.7em;
    font-weight: 700;
  }
`;

const MatchFinderPage = () => {
  const [displayedMatches, setDisplayedMatches] = useState([]);
  const [searchActive, setSearchActive] = useState(false);
  const [loading, setLoading] = useState(true);
  const [notification, setNotification] = useState(false);
  const { auth } = useContext(AuthContext);
  const userDogIds = auth.dogs.map((dog) => dog._id);

  const searchEvents = (searchValue) => {
    const filteredMatches = displayedMatches.filter((match) =>
      Object.keys(match)
        .filter((key) => key === "dogName" || key === "age")
        .some((key) => {
          const valueString = match[key].toString().toLowerCase();
          return valueString.includes(searchValue.toLowerCase());
        })
    );
    return filteredMatches;
  };

  const handleSearch = (searchValue) => {
    const searchResults = searchEvents(searchValue);
    setSearchActive(true);
    setDisplayedMatches(searchResults);
  };

  const handleClearSearch = () => {
    setSearchActive(false);
    fetchAllDogs();
  };

  const displayNotification = () => {
    setNotification(true);
    setTimeout(() => setNotification(false), 2000);
  };

  const fetchAllDogs = async () => {
    try {
      const response = await fetch(`${API_ROOT}/dogs`);
      const data = await response.json();
      if (data.dogData) {
        // filter out user dogs
        const filteredDogs = data.dogData.filter(
          (dog) => !userDogIds.includes(dog._id)
        );
        setDisplayedMatches(filteredDogs);
      }
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAllDogs();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return <Loader />;
  }

  return (
    <MatchFinderContainer>
      <NavBar />
      <ContentContainer>
        <h1>Find a new play match</h1>
        <SearchBar
          searchActive={searchActive}
          triggerSearch={handleSearch}
          clearSearch={handleClearSearch}
        />
        <MatchGrid
          displayedMatches={displayedMatches}
          displayNotification={displayNotification}
          userDogs={auth.dogs}
        />
      </ContentContainer>
      {notification && <SavedConfirmation text={"Saved"} />}
    </MatchFinderContainer>
  );
};

export default MatchFinderPage;
