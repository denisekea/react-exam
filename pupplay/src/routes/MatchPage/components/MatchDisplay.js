import React from "react";
import styled from "styled-components";

const MatchContainer = styled("div")`
  box-sizing: border-box;
  width: 53%;
  margin: 2em auto;
  padding: 1em 1.5em;
  background-color: white;
  border-radius: 10px;
  text-align: center;

  h2 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.indigo};
    margin: 1em 0 0.5em;
  }

  h3 {
    color: ${(props) => props.theme.indigo};
    font-size: 1.05em;
    margin: 0;
    margin-bottom: 1.5em;
  }

  img {
    height: 170px;
    width: 170px;
    border-radius: 7px;
    object-fit: cover;
    margin: 0 1em;
  }

  button {
    display: block;
    margin: 2em auto 1em;
  }

  .dateDisplay {
    font-weight: bold;
    font-size: 1.2em;
  }
`;

const MatchDisplay = ({ auth, match, handleAcceptMatch }) => {
  const { dog1, dog2, playTime, accepted } = match;
  const parsedDate = new Date(playTime).toLocaleDateString(undefined, {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  });
  const parsedTime = new Date(playTime).toLocaleTimeString([], {
    hour12: false,
    hour: "2-digit",
    minute: "2-digit",
  });

  // dog1 is requesting dog
  const receivingUser = dog2.userid === auth._id;

  // if auth._id is dog2.userid and accepted=false then show accept button

  return (
    <MatchContainer>
      <h2>
        {dog1.dogName} vs. {dog2.dogName}
      </h2>
      <h3>
        {accepted
          ? "Confirmed"
          : receivingUser
          ? "You haven't accepted yet"
          : `${dog2.dogName} has not accepted yet`}
      </h3>
      <img src={dog1.imageURL} alt="dog 1" />
      <img src={dog2.imageURL} alt="dog 2" />
      <h2>When?</h2>
      <p className="dateDisplay">
        {parsedDate}
        <br />
        at {parsedTime}
      </p>
      {!accepted && receivingUser && (
        <button onClick={handleAcceptMatch}>Accept</button>
      )}
    </MatchContainer>
  );
};

export default MatchDisplay;
