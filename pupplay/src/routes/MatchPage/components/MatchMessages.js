import React from "react";
import styled from "styled-components";
// import { io } from "socket.io-client";

// const socket = io("/matchchat");

const MessagesContainer = styled("div")`
  box-sizing: border-box;
  width: 53%;
  margin: 2em auto;
  padding: 1em 1.5em 2em;
  background-color: white;
  border-radius: 10px;
  text-align: center;

  h2 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.indigo};
    margin: 1em 0 0.5em;
  }
`;

const MessagesHistory = styled("div")`
  min-height: 5em;
  border-radius: 10px;
  background-color: #ececec;
`;

const MatchMessages = () => {
  //   const [isConnected, setIsConnected] = useState(socket.connected);
  //   const [lastMessage, setLastMessage] = useState(null);

  //   useEffect(() => {
  //     socket.on("connect", () => {
  //       setIsConnected(true);
  //     });
  //     socket.on("disconnect", () => {
  //       setIsConnected(false);
  //     });
  //     socket.on("message", (data) => {
  //       console.log("received in frontend");
  //       setLastMessage(data);
  //     });
  //     return () => {
  //       socket.off("connect");
  //       socket.off("disconnect");
  //       socket.off("message");
  //     };
  //   });

  //   const sendMessage = () => {
  //     console.log("sendMessage");
  //     socket.emit("message", { data: "hello from frontend!" });
  //   };

  return (
    <MessagesContainer>
      <h2>Messages</h2>
      <MessagesHistory></MessagesHistory>
      {/* <p>Connected: {"" + isConnected}</p>
      <p>Last message: {lastMessage || "-"}</p> */}
      <button>Say hello!</button>
    </MessagesContainer>
  );
};

export default MatchMessages;
