import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import { useParams } from "react-router-dom";
import { API_ROOT } from "../../endpoints";
import AuthContext from "../../common/AuthContext/AuthContext";
import NavBar from "../../common/Navigation/NavBar";
import Loader from "../../common/Loader/Loader";
import MatchDisplay from "./components/MatchDisplay";
import MatchMessages from "./components/MatchMessages";

const MatchFinderContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;
`;

const ContentContainer = styled("div")`
  padding: 0 7% 3em;

  h1 {
    font-family: "Montserrat", sans-serif;
    color: ${(props) => props.theme.headerColor};
    margin: 0;
    margin-bottom: 1em;
    font-size: 2.7em;
    font-weight: 700;
  }
`;

const MatchPage = () => {
  const [match, setMatch] = useState();
  const [loading, setLoading] = useState(true);
  const { auth } = useContext(AuthContext);
  let { matchid } = useParams();
  console.log(match);

  // TODO: restrict access to users of both dogs

  const handleAcceptMatch = async () => {
    try {
      const response = await fetch(`${API_ROOT}/matches/${matchid}`, {
        method: "POST",
        body: JSON.stringify({ accepted: true }),
        header: { "Content-type": "application/json" },
      });
      const data = await response.json();
      if (data.matchData) {
        setMatch(data.matchData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const fetchMatch = async () => {
      try {
        const response = await fetch(`${API_ROOT}/matches/${matchid}`);
        const data = await response.json();
        if (data.matchData) {
          setMatch(data.matchData);
          setLoading(false);
        }
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    };

    fetchMatch();
  }, [matchid]);

  if (loading) {
    return <Loader />;
  }

  return (
    <MatchFinderContainer>
      <NavBar />
      <ContentContainer>
        <h1>Match page</h1>
        <MatchDisplay
          auth={auth}
          match={match}
          handleAcceptMatch={handleAcceptMatch}
        />
        <MatchMessages />
      </ContentContainer>
    </MatchFinderContainer>
  );
};

export default MatchPage;
