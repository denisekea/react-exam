import React, { useState } from "react";
import styled from "styled-components";
import SavedConfirmation from "../../common/Notification/SavedConfirmation";
import TabSwitch from "./components/TabSwitch";
import LoginForm from "./components/LoginForm";
import SignupForm from "./components/SignupForm";

const LoginContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TabContainer = styled("div")`
  background-color: white;
  height: 65%;
  width: 50%;
  max-width: 500px;
  border-radius: 10px;
`;

const LoginPage = (props) => {
  const [view, setView] = useState("login");
  const [notification, setNotification] = useState(false);

  const changeView = (newView) => {
    setView(newView);
  };

  const displayNotification = () => {
    setNotification(true);
    setTimeout(() => setNotification(false), 2300);
  };

  return (
    <LoginContainer>
      <TabContainer>
        <TabSwitch view={view} changeView={changeView} />
        {view === "login" ? (
          <LoginForm history={props.history} />
        ) : (
          <SignupForm
            changeView={changeView}
            displayNotification={displayNotification}
          />
        )}
      </TabContainer>
      {notification && <SavedConfirmation text={"You're signed up!"} />}
    </LoginContainer>
  );
};

export default LoginPage;
