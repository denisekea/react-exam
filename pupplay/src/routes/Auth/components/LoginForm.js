import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { API_ROOT } from "../../../endpoints";
import AuthContext from "../../../common/AuthContext/AuthContext";
import { FormContainer, ErrorDisplay } from "./form.styled";

const LoginForm = () => {
  const [errorMsg, setErrorMsg] = useState("");
  const { handleAuthChange } = useContext(AuthContext);
  const history = useHistory();

  const handleLogin = async (e) => {
    e.preventDefault();
    const form = document.querySelector("#loginForm");

    const connection = await fetch(`${API_ROOT}/auth/login`, {
      method: "POST",
      body: new FormData(form),
    });
    const data = await connection.json();

    if (data.user) {
      setErrorMsg("");
      handleAuthChange(data.user);
      localStorage.setItem("uid", data.user._id);
      history.push("/dashboard");
    } else {
      setErrorMsg(data.error);
    }
  };

  return (
    <FormContainer id="loginForm" method="POST" onSubmit={handleLogin}>
      <label htmlFor="email">Email</label>
      <input name="email" type="email" required />
      <label htmlFor="password">Password</label>
      <input name="password" type="password" required />
      <button>Log in</button>
      <ErrorDisplay>{errorMsg}</ErrorDisplay>
    </FormContainer>
  );
};

export default LoginForm;
