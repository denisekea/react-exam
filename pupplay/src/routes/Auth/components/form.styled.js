import styled from "styled-components";

export const FormContainer = styled("form")`
  width: 70%;
  margin: 1em auto;
  padding: 2em;

  input {
    box-sizing: border-box;
    display: block;
    height: 35px;
    width: 100%;
    margin: 0.4em 0 1.3em;
    font-family: "Raleway", sans-serif;
    font-size: 0.9em;
    border: none;
    border-radius: 5px;
    padding: 2px 5px;
    background-color: #efeafd;

    &:focus {
      outline: none;
      box-shadow: 0 0 0 2px #69f6b7;
    }
  }

  button {
    height: 30px;
    font-family: "Raleway", sans-serif;
    font-weight: bold;
    font-size: 0.9em;
    background-color: #69f6b7;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    padding: 4px 15px;
    margin-top: 0.5em;

    &:hover {
      background-color: #a6f9d4;
    }

    &:focus {
      outline: none;
    }
  }
`;

export const ErrorDisplay = styled("div")`
  color: red;
  margin-top: 1.2em;
  padding: 5px 0;
`;
